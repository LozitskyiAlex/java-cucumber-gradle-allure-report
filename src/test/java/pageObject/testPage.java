package pageObject;

import support.PropertiesCache;

import java.util.Objects;

import static com.codeborne.selenide.Selenide.open;

public class testPage {

    public static void openHomePage() {
        String url;

        if (Objects.equals(System.getProperty("url"), "") || System.getProperty("url") == null) {
            url = PropertiesCache.getInstance().getProperty("url");
        } else {
            url = System.getProperty("url");
        }
        open(url);
    }

    public static void waitFor(int second) throws InterruptedException {
        Thread.sleep(second * 1000);
    }
}
