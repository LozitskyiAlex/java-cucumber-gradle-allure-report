import com.codeborne.selenide.logevents.SelenideLogger;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import io.qameta.allure.selenide.AllureSelenide;
import io.qameta.allure.selenide.LogType;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import java.util.Objects;
import java.util.logging.Level;

import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;

@CucumberOptions(
        features = "src/test/resources/features",
        glue = {"stepdefs"},
        plugin = {
                "pretty",
                "io.qameta.allure.cucumber7jvm.AllureCucumber7Jvm",
        })
public class TestRunner extends AbstractTestNGCucumberTests {

    AllureSelenide allureSelenide;

    @Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() {
        return super.scenarios();
    }

    @BeforeClass
    public void setupClassName(ITestContext context) {
        if(System.getProperty("parallel").equals("true")) {
            if (Objects.equals(System.getProperty("threadCount"), "") || System.getProperty("threadCount") == null) {
                context.getCurrentXmlTest().getSuite().setDataProviderThreadCount(2);
            } else {
                context.getCurrentXmlTest().getSuite().setDataProviderThreadCount(Integer.parseInt(System.getProperty("threadCount")));
            }
        } else {
            context.getCurrentXmlTest().getSuite().setDataProviderThreadCount(1);
        }
        context.getCurrentXmlTest().getSuite().setPreserveOrder(false);
    }

    @BeforeClass
    public void setUp() {
        System.setProperty("chromeoptions.args", "--disable-gpu");
        System.setProperty("chromeoptions.args", "--no-sandbox");
        System.setProperty("chromeoptions.args", "--disable-dev-shm-usage");
    }

    @BeforeMethod
    public void addAllureListener() {
        allureSelenide = new AllureSelenide().enableLogs(LogType.DRIVER, Level.ALL);
        SelenideLogger.addListener("allure", allureSelenide);
    }

    @AfterClass
    public static void logout() {
        closeWebDriver();
        SelenideLogger.removeListener("allure");
    }
}
